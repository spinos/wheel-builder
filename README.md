# A builder of Python wheels for Openstack on Alpine Linux

This Docker image is used to build Python wheels for an Openstack release
targeting Alpine Linux. It works by downloading the requirements.txt,
global-requirements.txt and upper-constraints.txt from the
openstack/requirements project in order to find all Openstack
dependencies. Next pip is used to create wheels for every package in
requirements.txt. After the dependencies are built, pip is used to build
wheels for every Openstack project listed in projects.txt. Patches and
workarounds to make the projects and dependencies run on Alpine Linux
(and musl) are included.

## Openstack Projects and Dependencies

projects.txt            - list of Openstack projects to build wheels for
requirements.txt        - list of Opentstack project dependencies
upper-constraints.txt   - list of maximum version numbers for dependencies

Openstack requirements project:

https://github.com/openstack/requirements

Openstack Release Info:

https://releases.openstack.org/ocata/index.html

## Build Instructions

Build container

```
make image
```

Tag container

```
make tag
```

Login to Docker Hub

```
docker login
```

Push container to Docker Hub

```
make push
```

Set BUILD environment variable

BUILD  must contain the build output from running the
`spinos/wheel-builder` container:

```
$HOME
  |
    +-build
        |
        +-spinos
        |
        +-<ALPINE_BASE_VERSION>
        |
        +-<OPENSTACK_RELEASE_NAME>
          |
          +-wheels
```

For example:
$HOME/build/spinos/3.9/ocata/wheels

Build wheels

```
make wheels
```

## TODO

1. Create apk for liberasurecode https://github.com/openstack/liberasurecode
2. Fix pyeclib to build on Alpine
3. Re-evaluate pifpaf in requirements.txt it installs on Alpine 3.9

## Building liberasure library

```
apk add autoconf
apk add automake
apk add libtool
git clone https://github.com/openstack/liberasurecode.git
cd liberasurecode
./autogen.sh
./configure --prefix=/usr
make
make install
```
https://github.com/openstack/liberasurecode


## Building pyeclib package

Pyeclib won't install properly because it uses ctypes.util.find_library to
return the path of the liberasurecode.so library. However, on Alpine the
ldconfig application does not return the full path and setup.py in Pyeclib
expects a full path when calling ctypes.util.find_library(). A simple fix
is to comment out this library check in setup.py. A better fix would be to
apply a patch that adds a check to '_find_library' that will work without a
full path.

pip download pyeclib
tar zxvf /wheels/pyeclib-1.5.0.tar.gz -C /build
cd pyeclib-1.5.0/
<patch setup.py>
python setup.py bdist_wheel


Issues related to ctypes.util.find_library on Alpine:

https://bugs.python.org/issue21622
https://github.com/alpinelinux/aports/blob/master/main/python2/musl-find_library.patch
https://github.com/docker-library/python/issues/111


## Building mysql-python

Need to patch /usr/include/mysql.h

See:

https://github.com/DefectDojo/django-DefectDojo/issues/407
https://lists.launchpad.net/maria-developers/msg10744.html


