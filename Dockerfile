ARG BASE
ARG RELEASE
FROM $BASE

ENV RELEASE=$RELEASE

RUN apk -U add \
    build-base \
    git \
    python \
    py-pip \
    py-yaml \
    jpeg-dev \
    jq \
    openssl \
    libffi-dev \
    openssl-dev \
    libvirt-dev \
    libxml2-dev \
    libxslt-dev \
    linux-headers \
    mariadb-dev \
    nss-dev \
    openldap-dev \
    openblas-dev \
    python-dev \
    postgresql-dev \
    && true

#Symlink locale.h to make compiling with musl work for some python modules
RUN ln -s /usr/include/locale.h /usr/include/xlocale.h

#Create some volume mount points
RUN mkdir -p /build /site-packages /wheels

#Add docker volume mount point to site-packages path search list
RUN echo "/site-packages/" > /usr/lib/python2.7/site-packages/volumes.pth

#Install global pip settings
COPY pip.conf /etc/

#Install Openstack upper constraints file
COPY upper-constraints.txt /build/upper-constraints.txt
COPY global-requirements.txt /build/global-requirements.txt

COPY requirements.txt /build/requirements.txt
COPY build-requirements.txt /build/build-requirements.txt
COPY projects.txt /build/projects.txt
COPY Makefile.wheels /build/Makefile
COPY usr/bin/ /usr/bin/
COPY Dockerfile /
COPY entrypoint.sh /

#Install patch to fix mysql-python build on newer mariadb-dev versions
RUN sed '/st_mysql_options options;/a unsigned int reconnect;' /usr/include/mysql/mysql.h -i.orig

VOLUME /wheels
VOLUME /site-packages

#RUN pip install packaging wheel

WORKDIR /
ENTRYPOINT ["/entrypoint.sh"]
