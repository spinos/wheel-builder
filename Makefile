.PHONY: clean download image push tag wheels

#Docker org for this image
ORG?=spinos

#Docker repo name for this image
IMAGE=wheel-builder

#Openstack release name
RELEASE=ocata

#Docker base image
BASENAME=alpine
BASEVER=3.9
BASE=$(BASENAME):$(BASEVER)

WHEELS=$(BUILD)/spinos/$(BASEVER)/$(RELEASE)/wheels
SITE-PACKAGES=$(BUILD)/spinos/$(BASEVER)/$(RELEASE)/sitepackages

#Docker image dependencies
DEPS=usr/bin/build.sh usr/bin/install-build-deps.sh pip.conf projects.txt \
		 requirements.txt upper-constraints.txt global-requirements.txt \
		 Makefile.wheels

default: tag

download:
	wget -q -O upper-constraints.orig https://raw.githubusercontent.com/openstack/requirements/stable/${RELEASE}/upper-constraints.txt
	wget -q -O global-requirements.orig https://raw.githubusercontent.com/openstack/requirements/stable/${RELEASE}/global-requirements.txt
	cat upper-constraints.txt | sed 's/===[0-9a-z.+]*//' > requirements.orig

iid: Makefile Dockerfile $(DEPS)
	docker build --build-arg BASE=$(BASE) --build-arg RELEASE=$(RELEASE) --iidfile iid .

hash: iid
	docker run --rm --entrypoint '' $(shell cat iid) /bin/sh -c 'echo Dockerfile /lib/apk/db/installed | xargs cat | sha1sum' | sed 's/ .*//' > $@

image: hash

tag: image
	docker tag $(shell cat iid) $(ORG)/$(IMAGE):$(shell cat hash)
	docker tag $(shell cat iid) $(ORG)/$(IMAGE):$(RELEASE)
	docker tag $(shell cat iid) $(ORG)/$(IMAGE):latest

wheels.txt: tag
	@mkdir -p $(WHEELS) $(SITE-PACKAGES)
	docker run -t --rm --volume $(WHEELS):/wheels $(ORG)/$(IMAGE):$(shell cat hash)
	@find $(WHEELS) -name '*.whl' -exec basename {} \; | sort | tee wheels.txt

wheels: wheels.txt

push: tag
	docker push $(ORG)/$(IMAGE):$(shell cat hash)
	docker push $(ORG)/$(IMAGE):$(RELEASE)
	docker push $(ORG)/$(IMAGE):latest

cli:
	@mkdir -p $(WHEELS) $(SITE-PACKAGES)
	docker run -it --rm --volume $(WHEELS):/wheels $(ORG)/$(IMAGE):$(shell cat hash) /bin/sh

clean:
	rm -rf hash iid wheels.txt $(WHEELS) $(SITE-PACKAGES)
