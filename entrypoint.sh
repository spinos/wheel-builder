#!/bin/sh


if [ $# -eq 0 ]; then
  cd /build && make
else
  exec $@
fi
