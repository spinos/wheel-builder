
# Updates for Alpine 3.9

The following changes to upper-constraints.txt were necessary to get the
packages to install on Alpine 3.9. The previous changes from Alpine 3.6
are still applied.

psycopg2===2.7.7
cryptogrpahy===2.0.3


# Updates for Alpine 3.6

The following changes to upper-constraints.txt were necessary to get the
packages to install on Python 3.6.

cryptography===1.9
libvirt-python===3.0.0
python-nss===1.0.1
xattr===0.9.2
