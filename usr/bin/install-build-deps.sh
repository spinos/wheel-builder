#!/bin/sh

echo "Installing build dependencies..."
for pkg in $(cat /build/build-requirements.txt); do
  if ! pip show -q $pkg; then
    echo "Installing required package $pkg"
    pip download $pkg && pip install $pkg
  fi
done
