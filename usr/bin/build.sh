#!/bin/sh

archive_url="https://tarballs.openstack.org"

#Build wheels for Openstack dependencies
pip download -r /build/requirements.txt
pip wheel -r /build/requirements.txt

#Build wheels for Openstack service projects
for project in $(cat /build/projects.txt); do
  name=${project%%=*}
  version=${project##*=}
  archive="$archive_url/${name}/${name}-${version}.tar.gz"
  pip download $archive
  pip wheel "$name==$version"
done

